#Vending Machine

##You need to design a Vending Machine which

Accepts coins of 0.05€, 0.10€, 0.20€, 0.50€, 1€, 2€

Allow user to select products Coke(1.50€), Sprite(1.40€), Water(0.90€)

Allow user to take refund by canceling the request.

Return selected product and remaining change if any

Allow the vending machine supplier to refill beverages & coins for change.

##Constraints:

do not use databases. Save the state in memory.

do not use IoC frameworks (such as Spring or Guice)

make a rational use of libraries/dependencies (you can explain your decisions of why to include or not if you doubt whether you should or should not)

##Hints:

This is your cover letter, show us which are the quality standards that you consider a software application should accomplish.

Expose clearly the methods that provide the functionality asked, but there is no need to write a UI, a REST interface nor a CLI.

The aim of the test is to evaluate your ability to design a module that: it is easy to understand, test, reuse, evolve and maintain.