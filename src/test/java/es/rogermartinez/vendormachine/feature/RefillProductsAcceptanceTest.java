package es.rogermartinez.vendormachine.feature;

import es.rogermartinez.vendormachine.actions.RefillProducts;
import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import es.rogermartinez.vendormachine.infrastructure.product.InMemoryProductRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class RefillProductsAcceptanceTest {

    private RefillProducts refillProducts;
    private ProductRepository productRepository;

    @Before
    public void setUp() {
        productRepository = new InMemoryProductRepository();
        refillProducts = new RefillProducts(productRepository);
    }

    @Test
    public void should_refill_products() {
        refillProducts.execute(products());

        Map<Product, Integer> stock = productRepository.getStock();

        assertThat(stock).contains(entry(Product.COKE, 60), entry(Product.SPRITE, 60));
    }

    private Map<Product, Integer> products() {
        Map<Product, Integer> products = new HashMap<>();
        products.put(Product.SPRITE, 10);
        products.put(Product.COKE, 10);
        return products;
    }

}
