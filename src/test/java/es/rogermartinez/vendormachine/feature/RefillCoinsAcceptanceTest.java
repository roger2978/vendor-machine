package es.rogermartinez.vendormachine.feature;

import es.rogermartinez.vendormachine.actions.RefillCoins;
import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.infrastructure.change.InMemoryChangeRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.entry;

public class RefillCoinsAcceptanceTest {

    private RefillCoins refillCoins;
    private ChangeRepository changeRepository;

    @Before
    public void setUp() {
        changeRepository = new InMemoryChangeRepository();
        refillCoins = new RefillCoins(changeRepository);
    }

    @Test
    public void should_refill_coins() {
        refillCoins.execute(coins());

        Map<Coin, Integer> stock = changeRepository.getStock();

        assertThat(stock).contains(entry(Coin.FIVE_CENTS, 60), entry(Coin.TWENTY_CENTS, 60));
    }

    private Map<Coin, Integer> coins() {
        Map<Coin, Integer> coins = new HashMap<>();
        coins.put(Coin.FIVE_CENTS, 10);
        coins.put(Coin.TWENTY_CENTS, 10);
        return coins;
    }

}
