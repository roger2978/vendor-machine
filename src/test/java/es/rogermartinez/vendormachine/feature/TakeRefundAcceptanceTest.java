package es.rogermartinez.vendormachine.feature;

import es.rogermartinez.vendormachine.actions.InsertCoin;
import es.rogermartinez.vendormachine.actions.TakeRefund;
import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.infrastructure.balance.InMemoryBalanceRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TakeRefundAcceptanceTest {

    private InsertCoin insertCoin;
    private BalanceRepository balanceRepository;
    private TakeRefund takeRefund;

    @Before
    public void setUp() {
        balanceRepository = new InMemoryBalanceRepository();
        insertCoin = new InsertCoin(balanceRepository);
        takeRefund = new TakeRefund(balanceRepository);
    }

    @Test
    public void should_take_refund() {
        insertCoin.execute(Coin.FIFTY_CENTS);
        insertCoin.execute(Coin.ONE_EURO);
        List<Coin> change = takeRefund.execute();

        Assertions
                .assertThat(change)
                .contains(Coin.FIFTY_CENTS, Coin.ONE_EURO);
    }
}
