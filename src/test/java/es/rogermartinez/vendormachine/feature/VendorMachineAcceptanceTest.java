package es.rogermartinez.vendormachine.feature;

import es.rogermartinez.vendormachine.actions.GetProduct;
import es.rogermartinez.vendormachine.actions.InsertCoin;
import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import es.rogermartinez.vendormachine.domain.purchase.Purchase;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;
import es.rogermartinez.vendormachine.infrastructure.balance.InMemoryBalanceRepository;
import es.rogermartinez.vendormachine.infrastructure.change.InMemoryChangeRepository;
import es.rogermartinez.vendormachine.infrastructure.product.InMemoryProductRepository;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

public class VendorMachineAcceptanceTest {

    private InsertCoin insertCoin;
    private GetProduct getProduct;
    private BalanceRepository balanceRepository;
    private ProductRepository productRepository;
    private ChangeRepository changeRepository;

    @Before
    public void setUp() {
        balanceRepository = new InMemoryBalanceRepository();
        productRepository = new InMemoryProductRepository();
        changeRepository = new InMemoryChangeRepository();
        insertCoin = new InsertCoin(balanceRepository);
        getProduct = new GetProduct(balanceRepository, productRepository, changeRepository);
    }

    @Test
    public void should_return_a_product_selected_and_change() throws VendorMachineException {
        insertCoin.execute(Coin.FIFTY_CENTS);
        insertCoin.execute(Coin.ONE_EURO);
        Purchase purchase = getProduct.execute(Product.SPRITE);

        Assertions
                .assertThat(purchase)
                .hasFieldOrPropertyWithValue("product", Product.SPRITE)
                .hasFieldOrPropertyWithValue("change", Collections.singletonList(Coin.TEN_CENTS));
    }
}
