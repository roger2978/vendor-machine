package es.rogermartinez.vendormachine.infrastructure.product;

import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.entry;

public class InMemoryProductRepositoryTest {

    private InMemoryProductRepository inMemoryProductRepository;

    @Before
    public void setUp() {
        inMemoryProductRepository = new InMemoryProductRepository();
    }

    @After
    public void tearDown() {
        inMemoryProductRepository.initialize();
    }

    @Test
    public void should_return_stock() throws VendorMachineException {
        inMemoryProductRepository.checkStock(Product.SPRITE);

        assertThat(inMemoryProductRepository.getStockFor(Product.SPRITE)).isEqualTo(49);
    }

    @Test
    public void should_fail_if_stock_is_0() {
        Map<Product, Integer> stock = new HashMap<>();
        stock.put(Product.SPRITE, 0);
        inMemoryProductRepository.initializeStock(stock);

        Throwable throwable = catchThrowable(() -> inMemoryProductRepository.checkStock(Product.SPRITE));

        assertThat(throwable).isInstanceOf(VendorMachineException.class)
                .hasMessage("Not enough stock");
    }

    @Test
    public void should_refill() {
        Map<Product, Integer> newStock = new HashMap<>();
        newStock.put(Product.SPRITE, 10);
        newStock.put(Product.COKE, 10);

        inMemoryProductRepository.refill(newStock);

        assertThat(inMemoryProductRepository.getStock())
                .contains(entry(Product.COKE, 60), entry(Product.SPRITE, 60));
    }
}