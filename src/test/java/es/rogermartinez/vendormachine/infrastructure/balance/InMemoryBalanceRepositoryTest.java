package es.rogermartinez.vendormachine.infrastructure.balance;

import es.rogermartinez.vendormachine.domain.share.Coin;
import org.junit.Before;
import org.junit.Test;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;

public class InMemoryBalanceRepositoryTest {

    private InMemoryBalanceRepository inMemoryBalanceRepository;

    @Before
    public void setUp() {
        inMemoryBalanceRepository = new InMemoryBalanceRepository();
    }

    @Test
    public void should_increase_balance() {
        inMemoryBalanceRepository.increase(Coin.FIFTY_CENTS);

        assertThat(inMemoryBalanceRepository.getBalance()).isEqualTo(50);
        assertThat(inMemoryBalanceRepository.getCoins()).isEqualTo(singletonList(Coin.FIFTY_CENTS));
    }

    @Test
    public void should_increase_balance_twice() {
        inMemoryBalanceRepository.increase(Coin.FIFTY_CENTS);
        inMemoryBalanceRepository.increase(Coin.ONE_EURO);

        assertThat(inMemoryBalanceRepository.getBalance()).isEqualTo(150);
        assertThat(inMemoryBalanceRepository.getCoins()).isEqualTo(asList(Coin.FIFTY_CENTS, Coin.ONE_EURO));
    }

    @Test
    public void should_reset_balance() {
        inMemoryBalanceRepository.increase(Coin.FIFTY_CENTS);
        inMemoryBalanceRepository.increase(Coin.ONE_EURO);

        inMemoryBalanceRepository.reset();

        assertThat(inMemoryBalanceRepository.getBalance()).isEqualTo(0);
        assertThat(inMemoryBalanceRepository.getCoins()).isEmpty();
    }
}