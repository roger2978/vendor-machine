package es.rogermartinez.vendormachine.infrastructure.change;

import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.Assertions.entry;

public class InMemoryChangeRepositoryTest {

    private InMemoryChangeRepository inMemoryChangeRepository;

    @Before
    public void setUp() {
        inMemoryChangeRepository = new InMemoryChangeRepository();
    }

    @After
    public void tearDown() {
        inMemoryChangeRepository.initialize();
    }

    @Test
    public void should_return_change() throws VendorMachineException {
        List<Coin> change = inMemoryChangeRepository.getChange(10);

        assertThat(change).contains(Coin.TEN_CENTS);
    }

    @Test
    public void should_return_2_coins_of_5() throws VendorMachineException {
        Map<Coin, Integer> stock = new HashMap<>();
        stock.put(Coin.TEN_CENTS, 0);
        stock.put(Coin.FIVE_CENTS, 2);
        inMemoryChangeRepository.initializeStock(stock);

        List<Coin> change = inMemoryChangeRepository.getChange(10);

        assertThat(change).contains(Coin.FIVE_CENTS, Coin.FIVE_CENTS);
    }

    @Test
    public void should_fail_if_no_change() {
        Map<Coin, Integer> stock = new HashMap<>();
        stock.put(Coin.TEN_CENTS, 0);
        stock.put(Coin.FIVE_CENTS, 1);
        inMemoryChangeRepository.initializeStock(stock);

        Throwable throwable = catchThrowable(() -> inMemoryChangeRepository.getChange(10));

        assertThat(throwable).isInstanceOf(VendorMachineException.class)
                .hasMessage("Not enough change");
    }

    @Test
    public void should_refill() {
        Map<Coin, Integer> newStock = new HashMap<>();
        newStock.put(Coin.ONE_EURO, 10);
        newStock.put(Coin.FIFTY_CENTS, 10);

        inMemoryChangeRepository.refill(newStock);

        assertThat(inMemoryChangeRepository.getStock())
                .contains(entry(Coin.ONE_EURO, 60), entry(Coin.FIFTY_CENTS, 60));
    }
}