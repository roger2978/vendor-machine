package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class InsertCoinTest {

    @Mock
    private BalanceRepository balanceRepository;

    private InsertCoin insertCoin;

    @Before
    public void setUp() {
        insertCoin = new InsertCoin(balanceRepository);
    }

    @Test
    public void should_increase_balance() {
        insertCoin.execute(Coin.FIFTY_CENTS);

        verify(balanceRepository).increase(Coin.FIFTY_CENTS);
    }
}