package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RefillProductsTest {

    @Mock
    private ProductRepository productRepository;

    private RefillProducts refillProducts;

    @Before
    public void setUp() {
        refillProducts = new RefillProducts(productRepository);
    }

    @Test
    public void should_refill_products() {
        Map<Product, Integer> products = products();

        refillProducts.execute(products);

        verify(productRepository).refill(products);
    }

    private Map<Product, Integer> products() {
        Map<Product, Integer> products = new HashMap<>();
        products.put(Product.SPRITE, 10);
        products.put(Product.COKE, 10);
        return products;
    }
}