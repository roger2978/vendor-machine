package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TakeRefundTest {

    @Mock
    private BalanceRepository balanceRepository;

    private TakeRefund takeRefund;

    @Before
    public void setUp() {
        takeRefund = new TakeRefund(balanceRepository);
    }

    @Test
    public void should_return_refund() {
        given(balanceRepository.getCoins()).willReturn(Arrays.asList(Coin.FIFTY_CENTS, Coin.TWENTY_CENTS));

        List<Coin> change = takeRefund.execute();

        verify(balanceRepository).reset();
        assertThat(change).contains(Coin.FIFTY_CENTS, Coin.TWENTY_CENTS);

    }
}