package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import es.rogermartinez.vendormachine.domain.purchase.Purchase;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.doThrow;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class GetProductTest {

    @Mock
    private BalanceRepository balanceRepository;
    @Mock
    private ProductRepository productRepository;
    @Mock
    private ChangeRepository changeRepository;

    private GetProduct getProduct;

    @Before
    public void setUp() {
        getProduct = new GetProduct(balanceRepository, productRepository, changeRepository);
    }

    @Test
    public void should_fail_if_balance_is_not_enough() {
        given(balanceRepository.getBalance()).willReturn(50);

        Throwable throwable = catchThrowable(() -> getProduct.execute(Product.SPRITE));

        assertThat(throwable).isInstanceOf(VendorMachineException.class)
                .hasMessage("Not enough balance");
    }

    @Test
    public void should_fail_if_stock_is_not_enough() throws VendorMachineException {
        given(balanceRepository.getBalance()).willReturn(150);
        doThrow(new VendorMachineException("Not enough stock")).when(productRepository).checkStock(Product.SPRITE);

        Throwable throwable = catchThrowable(() -> getProduct.execute(Product.SPRITE));

        assertThat(throwable).isInstanceOf(VendorMachineException.class)
                .hasMessage("Not enough stock");
    }

    @Test
    public void should_fail_if_change_is_not_enough() throws VendorMachineException {
        given(balanceRepository.getBalance()).willReturn(150);
        given(changeRepository.getChange(10)).willThrow(new VendorMachineException("Not enough change"));

        Throwable throwable = catchThrowable(() -> getProduct.execute(Product.SPRITE));

        assertThat(throwable).isInstanceOf(VendorMachineException.class)
                .hasMessage("Not enough change");
    }

    @Test
    public void should_return_the_product_and_change() throws VendorMachineException {
        given(balanceRepository.getBalance()).willReturn(150);
        given(changeRepository.getChange(10)).willReturn(Collections.singletonList(Coin.TEN_CENTS));

        Purchase purchase = getProduct.execute(Product.SPRITE);

        verify(balanceRepository).reset();
        assertThat(purchase)
                .hasFieldOrPropertyWithValue("product", Product.SPRITE)
                .hasFieldOrPropertyWithValue("change", Collections.singletonList(Coin.TEN_CENTS));
    }
}