package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class RefillCoinsTest {

    @Mock
    private ChangeRepository changeRepository;

    private RefillCoins refillCoins;

    @Before
    public void setUp() {
        refillCoins = new RefillCoins(changeRepository);
    }

    @Test
    public void should_refill_products() {
        Map<Coin, Integer> coins = coins();

        refillCoins.execute(coins);

        verify(changeRepository).refill(coins);
    }

    private Map<Coin, Integer> coins() {
        Map<Coin, Integer> products = new HashMap<>();
        products.put(Coin.FIFTY_CENTS, 10);
        products.put(Coin.ONE_EURO, 10);
        return products;
    }
}