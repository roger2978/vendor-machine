package es.rogermartinez.vendormachine.infrastructure.product;

import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;

import java.util.HashMap;
import java.util.Map;

public class InMemoryProductRepository implements ProductRepository {

    private static Map<Product, Integer> stock;

    public InMemoryProductRepository() {
        if (stock == null) {
            initialize();
        }
    }

    @Override
    public void checkStock(Product sprite) throws VendorMachineException {
        Integer quantity = stock.get(sprite);
        if (quantity == 0) {
            throw new VendorMachineException("Not enough stock");
        }
        stock.put(sprite, --quantity);
    }

    @Override
    public Map<Product, Integer> getStock() {
        return stock;
    }

    @Override
    public void refill(Map<Product, Integer> products) {
        products
                .keySet()
                .forEach(product -> stock.put(product, stock.get(product) + products.get(product)));
    }

    // Only for testing
    void initializeStock(Map<Product, Integer> initialStock) {
        stock = initialStock;
    }

    // Only for test
    void initialize() {
        Map<Product, Integer> initialStock = new HashMap<>();
        initialStock.put(Product.SPRITE, 50);
        initialStock.put(Product.COKE, 50);
        initialStock.put(Product.WATER, 50);
        initializeStock(initialStock);
    }

    // Only for testing
    int getStockFor(Product sprite) {
        return stock.get(sprite);
    }
}
