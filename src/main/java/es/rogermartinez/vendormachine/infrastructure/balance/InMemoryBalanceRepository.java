package es.rogermartinez.vendormachine.infrastructure.balance;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;

import java.util.ArrayList;
import java.util.List;

public class InMemoryBalanceRepository implements BalanceRepository {

    private static int balance;
    private static List<Coin> coins = new ArrayList<>();

    @Override
    public void increase(Coin coin) {
        balance += coin.getValue();
        coins.add(coin);
    }

    @Override
    public int getBalance() {
        return balance;
    }

    @Override
    public List<Coin> getCoins() {
        return coins;
    }

    @Override
    public void reset() {
        balance = 0;
        coins.clear();
    }
}
