package es.rogermartinez.vendormachine.infrastructure.change;

import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class InMemoryChangeRepository implements ChangeRepository {

    private static Map<Coin, Integer> stock;

    public InMemoryChangeRepository() {
        if (stock == null) {
            initialize();
        }
    }

    @Override
    public List<Coin> getChange(int change) throws VendorMachineException {
        int remainingChange = change;
        List<Coin> coins = new ArrayList<>();
        List<Coin> coinsOrdered = sortCoins();

        while (hasChange(remainingChange, coinsOrdered)) {
            Coin actualCoin = coinsOrdered.get(0);
            Integer numOfCoins = stock.get(actualCoin);
            if (hasCoin(remainingChange, actualCoin, numOfCoins)) {
                remainingChange = getRemainingChange(remainingChange, coins, actualCoin, numOfCoins);
            } else {
                coinsOrdered.remove(0);
            }
        }

        verifyChange(remainingChange, coins);
        return coins;
    }

    @Override
    public void refill(Map<Coin, Integer> coins) {
        coins
                .keySet()
                .forEach(coin -> stock.put(coin, stock.get(coin) + coins.get(coin)));

    }

    @Override
    public Map<Coin, Integer> getStock() {
        return stock;
    }

    private void verifyChange(int remainingChange, List<Coin> coins) throws VendorMachineException {
        if (remainingChange > 0) {
            refillStock(coins);
            throw new VendorMachineException("Not enough change");
        }
    }

    private void refillStock(List<Coin> coins) {
        coins.forEach(coin -> stock.put(coin, stock.get(coin) + 1));
    }

    private int getRemainingChange(int remainingChange, List<Coin> coins, Coin actualCoin, Integer numOfCoins) {
        coins.add(actualCoin);
        remainingChange = remainingChange - actualCoin.getValue();
        stock.put(actualCoin, --numOfCoins);
        return remainingChange;
    }

    private boolean hasCoin(int remainingChange, Coin actualCoin, Integer numOfCoins) {
        return remainingChange / actualCoin.getValue() > 0 && numOfCoins > 0;
    }

    private boolean hasChange(int remainingChange, List<Coin> coinsOrdered) {
        return remainingChange > 0 && coinsOrdered.size() > 0;
    }

    private List<Coin> sortCoins() {
        List<Coin> coinsOrdered = new ArrayList<>(Arrays.asList(Coin.values()));
        coinsOrdered.sort((o1, o2) -> (o1.getValue() < o2.getValue()) ? 1 : -1);
        return coinsOrdered;
    }

    // Only for testing
    void initializeStock(Map<Coin, Integer> initialStock) {
        stock = initialStock;
    }

    // Only for test
    void initialize() {
        Map<Coin, Integer> initialStock = new HashMap<>();
        initialStock.put(Coin.TWO_EUROS, 50);
        initialStock.put(Coin.ONE_EURO, 50);
        initialStock.put(Coin.FIFTY_CENTS, 50);
        initialStock.put(Coin.TWENTY_CENTS, 50);
        initialStock.put(Coin.TEN_CENTS, 50);
        initialStock.put(Coin.FIVE_CENTS, 50);
        initializeStock(initialStock);
    }
}
