package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;
import es.rogermartinez.vendormachine.domain.purchase.Purchase;
import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;

import java.util.List;

public class GetProduct {

    private BalanceRepository balanceRepository;
    private ProductRepository productRepository;
    private ChangeRepository changeRepository;

    public GetProduct(BalanceRepository balanceRepository, ProductRepository productRepository, ChangeRepository changeRepository) {
        this.balanceRepository = balanceRepository;
        this.productRepository = productRepository;
        this.changeRepository = changeRepository;
    }

    public Purchase execute(Product product) throws VendorMachineException {
        int balance = balanceRepository.getBalance();
        checkBalance(product, balance);
        productRepository.checkStock(product);
        balanceRepository.reset();
        List<Coin> change = changeRepository.getChange(balance - product.getPrice());
        return new Purchase(product, change);
    }

    private void checkBalance(Product product, int balance) throws VendorMachineException {
        if (balance < product.getPrice()) {
            throw new VendorMachineException("Not enough balance");
        }
    }
}
