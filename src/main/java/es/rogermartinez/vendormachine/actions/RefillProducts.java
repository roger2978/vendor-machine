package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.product.ProductRepository;

import java.util.Map;

public class RefillProducts {

    private ProductRepository productRepository;

    public RefillProducts(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void execute(Map<Product, Integer> products) {
        productRepository.refill(products);
    }
}
