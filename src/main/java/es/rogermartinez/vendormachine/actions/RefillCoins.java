package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.change.ChangeRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;

import java.util.Map;

public class RefillCoins {

    private ChangeRepository changeRepository;

    public RefillCoins(ChangeRepository productRepository) {
        this.changeRepository = productRepository;
    }

    public void execute(Map<Coin, Integer> coins) {
        changeRepository.refill(coins);
    }
}
