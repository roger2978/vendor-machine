package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;

public class InsertCoin {

    private BalanceRepository balanceRepository;

    public InsertCoin(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public void execute(Coin coin) {
        balanceRepository.increase(coin);
    }
}
