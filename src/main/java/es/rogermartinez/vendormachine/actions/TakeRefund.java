package es.rogermartinez.vendormachine.actions;

import es.rogermartinez.vendormachine.domain.balance.BalanceRepository;
import es.rogermartinez.vendormachine.domain.share.Coin;

import java.util.ArrayList;
import java.util.List;

public class TakeRefund {

    private BalanceRepository balanceRepository;

    public TakeRefund(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public List<Coin> execute() {
        List<Coin> coins = new ArrayList<>(balanceRepository.getCoins());
        balanceRepository.reset();
        return coins;
    }
}
