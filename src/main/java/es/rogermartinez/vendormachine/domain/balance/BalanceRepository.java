package es.rogermartinez.vendormachine.domain.balance;

import es.rogermartinez.vendormachine.domain.share.Coin;

import java.util.List;

public interface BalanceRepository {

    void increase(Coin fiftyCents);

    int getBalance();

    List<Coin> getCoins();

    void reset();
}
