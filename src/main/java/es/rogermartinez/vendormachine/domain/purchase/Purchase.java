package es.rogermartinez.vendormachine.domain.purchase;

import es.rogermartinez.vendormachine.domain.product.Product;
import es.rogermartinez.vendormachine.domain.share.Coin;

import java.util.List;

public class Purchase {

    private Product product;
    private List<Coin> change;

    public Purchase(Product product, List<Coin> change) {
        this.product = product;
        this.change = change;
    }

    public Product getProduct() {
        return product;
    }

    public List<Coin> getChange() {
        return change;
    }
}
