package es.rogermartinez.vendormachine.domain.share.exception;

public class VendorMachineException extends Exception {

    public VendorMachineException(String message) {
        super(message);
    }

}
