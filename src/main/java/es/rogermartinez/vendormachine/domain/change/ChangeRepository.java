package es.rogermartinez.vendormachine.domain.change;

import es.rogermartinez.vendormachine.domain.share.Coin;
import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;

import java.util.List;
import java.util.Map;

public interface ChangeRepository {
    List<Coin> getChange(int change) throws VendorMachineException;

    void refill(Map<Coin, Integer> coins);

    Map<Coin, Integer> getStock();
}
