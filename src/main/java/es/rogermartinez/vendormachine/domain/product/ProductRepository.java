package es.rogermartinez.vendormachine.domain.product;

import es.rogermartinez.vendormachine.domain.share.exception.VendorMachineException;

import java.util.Map;

public interface ProductRepository {
    void checkStock(Product sprite) throws VendorMachineException;

    Map<Product, Integer> getStock();

    void refill(Map<Product, Integer> products);
}
