package es.rogermartinez.vendormachine.domain.product;

public enum Product {

    COKE(150),
    SPRITE(140),
    WATER(90);

    private int price;

    Product(int price) {
        this.price = price;
    }

    public int getPrice() {
        return price;
    }
}
